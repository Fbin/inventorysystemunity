﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Inventory : MonoBehaviour {
    public int slotAmount;
    public List<Items> inventory = new List<Items>();
    public List<GameObject> slots = new List<GameObject>();
    public GameObject inventorySlot;
    public GameObject inventoryItem;
    public ItemDatabase database;

    private string itemDescription;
    private GameObject inventoryPanel;
    private GameObject slotPanel;


    void Start () {
        database = GetComponent<ItemDatabase>();
        inventoryPanel = GameObject.Find("Inventory Panel");
        slotPanel = inventoryPanel.transform.FindChild("Itemslots").gameObject;
        slotAmount = 9;
        for (int i = 0; i < slotAmount; i++)
        {
            inventory.Add(new Items());
            slots.Add(Instantiate(inventorySlot));
            slots[i].GetComponent<Slot>().id = i;
            slots[i].transform.SetParent(slotPanel.transform);
        }
        ItemDatabase.GenerateItems();
        AddItem(1);
        AddItem(1);
        AddItem(0);
        AddItem(2);
        AddItem(3);
        AddItem(2);
    }

    public void AddItem(int id)
    {
        Items addItem = database.GetItemById(id);
        if (addItem.itemStackable && InventoryContain(addItem))
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].itemID == id)
                {
                    ItemMovement data = slots[i].transform.GetChild(0).GetComponent<ItemMovement>();
                    data.stack++;
                    data.transform.GetChild(0).GetComponent<Text>().text = data.stack.ToString();
                    break;
                }
            }
        }
        else
        {
            for (int i = 0; i < inventory.Count; i++)
            {
                if (inventory[i].itemID == -1)
                {
                    inventory[i] = addItem;
                    GameObject itemObject = Instantiate(inventoryItem);
                    itemObject.GetComponent<ItemMovement>().item = addItem;
                    itemObject.GetComponent<ItemMovement>().stack = 1;
                    itemObject.GetComponent<ItemMovement>().slotloc = i;
                    itemObject.transform.SetParent(slots[i].transform);
                    itemObject.transform.position = Vector2.zero;
                    itemObject.GetComponent<Image>().sprite = addItem.itemIcon;
                    itemObject.name = addItem.itemName;
                    break;
                }
            }
        }
    }

    bool InventoryContain(Items item)
    {
        for (int i = 0; i < inventory.Count; i++)
        {
            if (inventory[i].itemID == item.itemID)
            {
                return true;
            }
        }
        return false;
    }
}
