﻿using UnityEngine;
using System.Collections;

public class ShowOrNot : MonoBehaviour {

    private GameObject inventar;
    private GameObject show;
    private GameObject characterPanel;
    private GameObject shop;

	void Start () {
        inventar = this.gameObject;
        show = inventar.transform.FindChild("Inventory Panel").gameObject;
        characterPanel = GameObject.Find("Character Panel");
        shop = GameObject.Find("Shop");
        characterPanel.SetActive(true);
        show.SetActive(true);
        shop.SetActive(false);
    }

    void Update()
    {
        if (Input.GetKeyDown("i"))
        {
            if (show.activeSelf)
            {
                show.SetActive(false);
            }

            else
            {
                show.SetActive(true);
                characterPanel.SetActive(true);
            }
        }

        if(Input.GetKeyDown("c"))
        {
            if (characterPanel.activeSelf)
            {
                characterPanel.SetActive(false);
            }

            else
            {
                characterPanel.SetActive(true);
            }
        }

        if (Input.GetKeyDown("s"))
        {
            if (shop.activeSelf)
            {
                shop.SetActive(false);
                characterPanel.SetActive(false);
                show.SetActive(false);
            }

            else
            {
                shop.SetActive(true);
            }
        }
    }
}
