﻿using UnityEngine;
using System.Collections;

public class Items : MonoBehaviour {
    public string itemName;
    public string itemDescription;
    public int itemValue;
    public int itemID;
    public Sprite itemIcon;
    public ItemType itemType;
    public bool itemStackable;

    private bool isDamaged;

    protected int itemEndurance;

    public bool IsDamaged
    {
        get
        {
            return isDamaged;
        }

        set
        {
            isDamaged = value;
        }
    }

    public enum ItemType
    {
        None, Weapon, Armor, Potion, QuestItem
    }

    public Items(string name, string description, int value, int id, bool stackable)
    {
        this.itemName = name;
        this.itemDescription = description;
        this.itemValue = value;
        this.itemID = id;
        this.itemStackable = stackable;
        this.itemIcon = Resources.Load<Sprite>("Sprites/Items/" + name);
    }

    public Items()
    {
        this.itemID = -1;
    }
}
