﻿using UnityEngine;
using System.Collections;

public class Armors : Items {
    public float itemDef;

    public Armors(int id, string name, string description, int value, int endurance, float def, bool stackable) : base(name, description, value, id, stackable)
    {
        this.itemDef = def;
        this.itemType = ItemType.Armor;
        this.itemStackable = stackable;
        this.itemEndurance = endurance;
    }
}
