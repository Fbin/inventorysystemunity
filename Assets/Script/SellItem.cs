﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;

public class SellItem : MonoBehaviour, IPointerDownHandler {

    public Items item;
    public int id;

    private Inventory inventory;
    private Sell saleBox;

    void Start () {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        saleBox = GameObject.Find("Sellslots").GetComponent<Sell>();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //obj doch nicht verkaufen
        //item wieder inventory hinzufügen??
        Destroy(saleBox.sellItems[id]);
        saleBox.sellItems.RemoveAt(id);
        saleBox.indexUpdate();
    }
}
