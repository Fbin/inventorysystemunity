﻿using UnityEngine;
using System.Collections;

public class Potions : Items {
    public float healAmount;
    public string itemNamePlural;
    public bool stackable;

    public Potions (int id, string name, string namePlural, string description, int value, float healing, bool stackable) : base(name, description, value, id, stackable)

    {
        this.itemNamePlural = namePlural;
        this.healAmount = healing;
        this.itemType = ItemType.Potion;
        this.itemEndurance = 1;
    }
}
