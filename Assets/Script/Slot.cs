﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System;

public class Slot : MonoBehaviour, IDropHandler {
    public int id;

    private Inventory inv;

    void Start () {
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();
	}

    public void OnDrop(PointerEventData eventData)
    {
        ItemMovement drop = eventData.pointerDrag.GetComponent<ItemMovement>();
        if (inv.inventory[id].itemID == -1)
        {
            inv.inventory[drop.slotloc] = new Items();
            inv.inventory[id] = drop.item;
            drop.slotloc = id;
        }
        else if(drop.slotloc != id)
        {
            //moving old item to old slot
            Transform swap = this.transform.GetChild(0);
            swap.GetComponent<ItemMovement>().slotloc = drop.slotloc;
            swap.transform.SetParent(inv.slots[drop.slotloc].transform);
            swap.transform.position = inv.slots[drop.slotloc].transform.position;

            //move drop item to this slot
            drop.slotloc = id;
            drop.transform.SetParent(this.transform);
            drop.transform.position = this.transform.position;

            inv.inventory[drop.slotloc] = swap.GetComponent<ItemMovement>().item;
            inv.inventory[id] = drop.item;
        }
    }

}
