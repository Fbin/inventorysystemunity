﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Tooltip : MonoBehaviour {
    private string text;
    private Items item;
    private GameObject tooltip;

    void Start()
    {
        tooltip = GameObject.Find("Tooltip");
        tooltip.SetActive(false);
    }

    void Update()
    {
        if (tooltip.activeSelf)
        {
            tooltip.transform.position = Input.mousePosition;
        }
    }

    public void Activate(Items item)
    {
        this.item = item;
        WriteTooltip();
        tooltip.SetActive(true);
    }

    public void Deactivate()
    {
        tooltip.SetActive(false);
    }

    public void WriteTooltip()
    {
        text = "<b>" + item.itemName + "</b>\n\n<size=10>" + item.itemDescription + "\nWert: " + item.itemValue + "</size>";
        tooltip.transform.GetChild(0).GetComponent<Text>().text = text;
    }
}
