﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using System;

public class Equipment : MonoBehaviour//, IPointerDownHandler
{
    public Items item;

    private Inventory inventory;
    private EquipmentSlot equipItemBox;
    
    // Use this for initialization
    void Start () {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        //wo sich obj befinden
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        //kann auf alten platz nicht mehr gelegt werden, andere Objekte schon
        Destroy(equipItemBox.equipment[0]);
        equipItemBox.equipment.RemoveAt(0);
    }
}
