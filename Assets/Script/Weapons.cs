﻿using UnityEngine;
using System.Collections;

public class Weapons : Items {
    public float itemMinDmg;
    public float itemMaxDmg;
    public float itemSpeed;

    public Weapons(int id, string name, string description, int value, float minDmg, float maxDmg, float speed, bool stackable) : base (name, description, value, id, stackable)
    {
        this.itemMinDmg = minDmg;
        this.itemMaxDmg = maxDmg;
        this.itemType = ItemType.Weapon;
        this.itemStackable = stackable;
        this.itemEndurance = 10;

        if (IsDamaged)
        {
            itemMaxDmg = maxDmg / 2;
        }
    }
}
