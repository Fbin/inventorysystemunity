﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using System.Collections.Generic;

public class EquipmentSlot : MonoBehaviour, IDropHandler
{
    public GameObject equipItem;
    public List<GameObject> equipment = new List<GameObject>();
    
    private Inventory inv;
    private ItemDatabase database;

    void Start()
    {
        inv = GameObject.Find("Inventory").GetComponent<Inventory>();
        database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
    }

    void Update()
    {
        if (equipment != null)
        {
            for (int i = 0; i < equipment.Count; i++)
            {
                transform.GetChild(0).GetComponent<Image>().enabled = true;
            }
        }
        else
        {
            transform.GetChild(0).GetComponent<Image>().enabled = false;
        }
    }

    public void OnDrop(PointerEventData eventData)
    {
        ItemMovement drop = eventData.pointerDrag.GetComponent<ItemMovement>();
        Items itemEquip = drop.item;
        int prevID = drop.item.itemID;
        int prevSlot = drop.slotloc;
        
        if (gameObject.tag == "Waffe" && itemEquip.itemType == Items.ItemType.Weapon)
        {
            toEquip(prevID, prevSlot);            
        }

        if (gameObject.tag == "Potion" && itemEquip.itemType == Items.ItemType.Potion)
        {
            toEquip(prevID, prevSlot);
            //anzahl der Tränke
        }

        if (gameObject.tag == "Rüstung" && itemEquip.itemType == Items.ItemType.Armor)
        {
            toEquip(prevID, prevSlot);
        }

        if (gameObject.tag == "Helm" && itemEquip.itemType == Items.ItemType.Armor)
        {
            toEquip(prevID, prevSlot);
        }

        if (gameObject.tag == "Hose" && itemEquip.itemType == Items.ItemType.Armor)
        {
            toEquip(prevID, prevSlot);
        }
        Destroy(eventData.pointerDrag);
    }

    public void toEquip(int id, int prevId)
    {
        GameObject item = (GameObject)Instantiate(equipItem);

        Equipment equip = item.GetComponent<Equipment>();

        equipment.Add(item);
        item.transform.SetParent(this.gameObject.transform);
        item.GetComponent<RectTransform>().localPosition = Vector2.zero;
        item.transform.GetComponent<Image>().sprite = database.GetItemById(id).itemIcon;

        //switch mit vorherObj
    } 
}
