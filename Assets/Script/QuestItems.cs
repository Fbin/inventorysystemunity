﻿using UnityEngine;
using System.Collections;

public class QuestItems : Items {

    public string itemNamePlural;
    public bool stackable;

    public QuestItems(int id, string name, string namePlural, string description, int value, int endurance, bool stackable) : base (name, description, value, id, stackable)
    {
        this.itemNamePlural = namePlural;
        this.itemType = ItemType.QuestItem;
        this.itemEndurance = endurance;
    }
}
