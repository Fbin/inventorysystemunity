﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System;
using System.Collections.Generic;

public class Sell : MonoBehaviour, IDropHandler {

    public List<GameObject> sellItems = new List<GameObject>();
    public GameObject sellItem;

    private Inventory inventory;
    private int prevID;
    private ItemDatabase database;

    void Start()
    {
        inventory = GameObject.Find("Inventory").GetComponent<Inventory>();
        database = GameObject.Find("Inventory").GetComponent<ItemDatabase>();
    }

    public void OnDrop(PointerEventData eventData)
    {
        ItemMovement drop = eventData.pointerDrag.GetComponent<ItemMovement>();
        int prevID = drop.item.itemID;
        int prevSlot = drop.slotloc;
        forSale(prevID, prevSlot);
    }

    public void forSale(int id, int prevId)
    {
        Vector2 pos = new Vector2(0, 65 - (30 * sellItems.Count));
        GameObject item = (GameObject)Instantiate(sellItem);

        SellItem sellIcon = item.GetComponent<SellItem>();
        sellIcon.id = sellItems.Count;

        sellItems.Add(item);
        item.transform.SetParent(this.gameObject.transform);
        item.GetComponent<RectTransform>().localPosition = pos;
        item.transform.GetChild(0).GetComponent<Image>().sprite = database.GetItemById(id).itemIcon;
        item.transform.GetChild(1).GetComponent<Text>().text = database.GetItemById(id).itemName;
        if (database.GetItemById(id).itemStackable)
        {
            for (int i = 0; i < inventory.inventory.Count; i++)
            {
                if (prevId == inventory.inventory[i].itemID)
                {
                    Debug.Log(inventory.inventory[prevId].itemName);
                    Debug.Log("### Existiertn Itemmovement: " + inventory.slots[prevId].transform.GetChild(0).GetComponent<ItemMovement>().stack);
                    //item.transform.GetChild(2).GetComponent<Text>().text = "x" + inventory.inventory[prevId].GetComponent<ItemMovement>().stack.ToString();

                    //item.transform.GetChild(2).GetComponent<Text>().enabled = true;
                }
            }
        }

        //original weg
    }

    public void indexUpdate()
    {
        for(int i=0; i < sellItems.Count; i++)
        {
            sellItems[i].GetComponent<SellItem>().id = i;
            Vector2 neuPos = new Vector2(0, 65 - (30 * i));
            sellItems[i].GetComponent<RectTransform>().localPosition = neuPos;
        }
    }
}
