﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ItemDatabase : MonoBehaviour {
    public static List<Items> items = new List<Items>();

    public static void GenerateItems()
    {
        items.Add(new Weapons(0,"Dolch", "Sticht mich etwas in die Seite?", 2, 2, 5, 5, false));
        items.Add(new Armors(1, "Unterhemd", "Etwas luftig, aber immerhin...", 1, 10, 1, false));
        items.Add(new Potions(2, "kleiner Heiltrank", "kleine Heiltränke", "Für den Durst zwischendurch.", 2, 5, true));
        items.Add(new QuestItems(3, "Klobrille", "Klobrillen", "Ein weisser Ring der Macht.", 0, 1, true));
    }

	void Start () {
        GenerateItems();
    }

    public Items GetItemById(int id)
    {
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i].itemID == id)
            {
                return items[i];
            }
        }
        return null;
    }
}